#  Swift boilerplate

This boilerplate includes:

- 💬 Swift 5.2
- 📦 Cocoapods 1.9.1
- 🧪 Quick 2.2
- 🥢 Nimble 8.0
