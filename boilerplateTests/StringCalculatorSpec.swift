//
//  boilerplateTests.swift
//  boilerplateTests
//
//  Created by Daniel Ramos on 26/04/2020.
//  Copyright © 2020 Acid Tango. All rights reserved.
//

import Quick
import Nimble

class StringCalculatorSpec: QuickSpec {
  override func spec() {
    describe("the first iteration") {
      it("returns the sum of two numbers") {
        let result = StringCalculator().add("1,2")
        expect(result).to(equal(3))
      }
    }
  }
}
